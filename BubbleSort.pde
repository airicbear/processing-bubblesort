private static Rect[] bars;

void settings() {
  size(500, 500);
}

void setup() {
  bars = new Rect[(int)(width / 2)];
  for (int i = 0; i < bars.length; i++) {
    bars[i] = new Rect(new Vector2(i * 10, height), new Vector2(10, -random(height * 4)));
    println(bars[i]);
  }
  bars = bubbleSort(bars);
}

void draw() {
  for (int i = 0; i < bars.length; i++) {
    rect(bars[i].position.x, bars[i].position.y, bars[i].scale.x, bars[i].scale.y);
  }
}

Rect[] bubbleSort(Rect[] array) {
  for (int i = 0; i < array.length; i++) {
    for (int j = i; j < array.length; j++) {
      if (abs(array[i].scale.y) > abs(array[j].scale.y)) {
        float temp = array[i].scale.y;
        array[i].scale.y = array[j].scale.y;
        array[j].scale.y = temp;
      }
    }
  }
  return array;
}

class Vector2 {
  float x, y;
  
  Vector2(float x, float y) {
    this.x = x;
    this.y = y;
  }
  
  String toString() {
    return "<" + this.x + ", " + this.y + ">";
  }
  
}

class Rect {
  Vector2 position, scale;
  
  Rect(Vector2 position, Vector2 scale) {
    this.position = position;
    this.scale = scale;
  }
  
  String toString() {
    return "Rect:\n  Position: " + this.position + "\n     Scale: " + this.scale;
  }
  
}
